package eu.specs.localdatamodel;

/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
@author  Pasquale De Rosa p.derosa@teamandroid.it
 */

import java.util.ArrayList;
import java.util.List;

public class CollectionSchemaMetrics {
    private String resource;
    private Integer total;
    private Integer members;
    private ArrayList<String> itemList;
    
    public CollectionSchemaMetrics(){
        //Zero arguments constructor
    }
    
    public CollectionSchemaMetrics(String resource, Integer total, Integer members, ArrayList<String> itemList){
        this.resource = resource;
        this.total = total;
        this.members = members;
        this.setItem(itemList);
    }

    public String getResource() {
        return resource;
    }
    public void setResource(String resource) {
        this.resource = resource;
    }
    public Integer getTotal() {
        return total;
    }
    public void setTotal(Integer total) {
        this.total = total;
    }
    public Integer getMembers() {
        return members;
    }
    public void setItems(Integer members) {
        this.members = members;
    }

    

	public ArrayList<String> getItem() {
		return itemList;
	}

	public void setItem(ArrayList<String> itemList) {
		this.itemList = itemList;
	}

	public void setMembers(Integer members) {
		this.members = members;
	}

}
