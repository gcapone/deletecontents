package eu.specs.localdatamodel;

public class SupplyChainCollection{
	private String total;

	private Item_list[] item_list;

	private String resource;

	private String members;

	public String getTotal ()
	{
		return total;
	}

	public void setTotal (String total)
	{
		this.total = total;
	}

	public Item_list[] getItem_list ()
	{
		return item_list;
	}

	public void setItem_list (Item_list[] item_list)
	{
		this.item_list = item_list;
	}

	public String getResource ()
	{
		return resource;
	}

	public void setResource (String resource)
	{
		this.resource = resource;
	}

	public String getMembers ()
	{
		return members;
	}

	public void setMembers (String members)
	{
		this.members = members;
	}

	@Override
	public String toString()
	{
		return "ClassPojo [total = "+total+", item_list = "+item_list+", resource = "+resource+", members = "+members+"]";
	}

	public class Item_list
	{
		private String id;

		private String item;

		public String getId ()
		{
			return id;
		}

		public void setId (String id)
		{
			this.id = id;
		}

		public String getItem ()
		{
			return item;
		}

		public void setItem (String item)
		{
			this.item = item;
		}

		@Override
		public String toString()
		{
			return "ClassPojo [id = "+id+", item = "+item+"]";
		}
	}

}