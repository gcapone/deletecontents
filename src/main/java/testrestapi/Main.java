package testrestapi;

import java.io.StringReader;
import java.io.StringWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Result;
import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.localdatamodel.CollectionSchema;
import eu.specs.localdatamodel.CollectionSchemaMetrics;
import eu.specs.localdatamodel.PlanActivityCollection;
import eu.specs.localdatamodel.SupplyChainCollection;
import eu.specsproject.slaplatform.slamanager.entities.Collection;


public class Main {

	//	private static String BASE_PATH ="http://apps.specs-project.eu";
	private static String BASE_PATH ="http://194.102.62.222:8080";
	//	private static String BASE_PATH ="http://194.102.62.242:8080";
	private static String SERVICE_MANAGER_PATH ="/service-manager/cloud-sla/security-mechanisms";
	private static String SLO_MANAGER_PATH ="/slo-manager-api/sla-negotiation/sla-templates/";
	private static String SLA_PATH ="/sla-manager/cloud-sla/slas?items=10000";
	private static String SUPPLY_CHAIN_PATH ="/planning-api/supply-chains/";
	private static String PLAN_ACTIVITIES_PATH ="/planning-api/plan-activities/";
	private static String SECURITY_REASONER_CAIQS ="/security-reasoner/caiqs";
	private static String SECURITY_REASONER_JUDGEMENT ="/security-reasoner/judgements";
	private static String SECURITY_METRICS ="/metric-catalogue/cloud-sla/security-metrics";


	public static void main(String[] args) {
		deleteAllSlas();
		deleteAllSupplyChains();
		deleteAllPlanActivites();
		//				deleteAllCaiqs();
		//				deleteAllJudgements();
		//				deleteAllMechanismsFromServiceManager();
		//				deleteAllSecurityMetrics();
		//				updateTemplatesWithViperId("123456");

	}


	public static void updateTemplatesWithViperId(String viperId){
		Client client = Client.create();

		WebResource webResource = client
				.resource(BASE_PATH+SLO_MANAGER_PATH);

		ClientResponse response = webResource
				.get(ClientResponse.class);

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}

		String xml = response.getEntity(String.class);

		System.out.println("Output from Server .... \n");
		System.out.println(xml);

		eu.specsproject.slo.Collection collection = buildCollectionSlosFromXml(xml);

		if(collection.itemList!=null && collection.itemList.size()>0){
			for(int i=0;i<collection.itemList.size();i++){
				System.out.println(i+ " - slo element: "+collection.itemList.get(i).itemValue);


			}
			client.destroy();
			Client client2 = Client.create();
			WebResource webResource2 = client2
					.resource(collection.itemList.get(0).itemValue);

			ClientResponse response2 = webResource2
					.get(ClientResponse.class);

			if (response2.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response2.getStatus());
			}

			String xml2 = response2.getEntity(String.class);
			System.out.println("Output from Server .... \n");
			System.out.println(xml2);

			AgreementOffer offer = buildSla(xml2);
			System.out.println(offer.getName());
			offer.setName(offer.getName()+"--ViperId_"+viperId);

			String xmlStringRepres = getStringFromAgreementOffer(offer).toString();
			System.out.println("xmlStringRepres:" + xmlStringRepres);

			client2.destroy();
			Client client3 = Client.create();
			WebResource webResource3 = client
					.resource(BASE_PATH+SLO_MANAGER_PATH);
			ClientResponse response3 = webResource3
					.type("text/xml")
					.post(ClientResponse.class, xmlStringRepres);
			if (response2.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response2.getStatus());
			}
		}
	}

	private static void deleteAllCaiqs() {
		Client client = Client.create();

		WebResource webResource = client
				.resource(BASE_PATH+SECURITY_REASONER_CAIQS);

		ClientResponse response = webResource.get(ClientResponse.class);

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}

		CollectionSchema collection = new Gson().fromJson(response.getEntity(String.class), CollectionSchema.class);
		if(collection!=null){
			for(int i=0;i<collection.getItem().size();i++){
				WebResource webResource2 = client
						.resource(collection.getItem().get(i).getValue());

				ClientResponse response2 = webResource2.delete(ClientResponse.class);

				if (response2.getStatus() != 204) {
					throw new RuntimeException("Failed : HTTP error code : "
							+ response2.getStatus());
				}
			}
		}
	}

	private static void deleteAllJudgements() {
		Client client = Client.create();

		WebResource webResource = client
				.resource(BASE_PATH+SECURITY_REASONER_JUDGEMENT);

		ClientResponse response = webResource.get(ClientResponse.class);

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}

		CollectionSchema collection = new Gson().fromJson(response.getEntity(String.class), CollectionSchema.class);
		if(collection!=null){
			for(int i=100;i<collection.getItem().size();i++){
				System.out.println("value: "+collection.getItem().get(i).getValue());
				WebResource webResource2 = client
						.resource(collection.getItem().get(i).getValue());

				ClientResponse response2 = webResource2.delete(ClientResponse.class);

				if (response2.getStatus() != 204) {
					throw new RuntimeException("Failed : HTTP error code : "
							+ response2.getStatus());
				}
			}
		}
	}

	private static void deleteAllSecurityMetrics() {
		Client client = Client.create();

		WebResource webResource = client
				.resource(BASE_PATH+SECURITY_METRICS);

		ClientResponse response = webResource.get(ClientResponse.class);

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}

		CollectionSchemaMetrics collection = new Gson().fromJson(response.getEntity(String.class), CollectionSchemaMetrics.class);
		if(collection!=null){
			int num = collection.getTotal();
			for (int j=0;j<num;j++){

				for(int i=0;i<collection.getMembers();i++){
					System.out.println("value: "+collection.getItem().get(i));
					WebResource webResource2 = client
							.resource(collection.getItem().get(i));

					ClientResponse response2 = webResource2.delete(ClientResponse.class);

					if (response2.getStatus() != 200) {
						throw new RuntimeException("Failed : HTTP error code : "
								+ response2.getStatus());
					}
					WebResource webResource3 = client
							.resource(BASE_PATH+SECURITY_METRICS);

					ClientResponse response3 = webResource3.get(ClientResponse.class);

					if (response3.getStatus() != 200) {
						throw new RuntimeException("Failed : HTTP error code : "
								+ response3.getStatus());
					}
					collection = new Gson().fromJson(response3.getEntity(String.class), CollectionSchemaMetrics.class);


				}
			}


		}
	}


	public static void deleteAllSlas(){
		Client client = Client.create();

		WebResource webResource = client
				.resource(BASE_PATH+SLA_PATH);

		ClientResponse response = webResource.accept("application/xml")
				.get(ClientResponse.class);

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}

		String xml = response.getEntity(String.class);

		System.out.println("Output from Server .... \n");
		System.out.println(xml);

		Collection collection = buildCollectionFromXml(xml);

		if(collection.item!=null && collection.item.size()>0){

			for(int i=0;i<collection.item.size();i++){
				if(collection.item.get(i).id.equalsIgnoreCase("573C5BB7E4B0929DB29CC56A")){
					System.out.println("the sla with id "+collection.item.get(i).id+" doesn't have to be deleted");
				}
				else{
					System.out.println(i+ " - sla deleted: "+collection.item.get(i).itemValue);

					WebResource webResource2 = client
							.resource(collection.item.get(i).itemValue);

					ClientResponse response2 = webResource2.accept("application/xml")
							.delete(ClientResponse.class);

					if (response2.getStatus() != 200) {
						throw new RuntimeException("Failed : HTTP error code : "
								+ response2.getStatus());
					}
				}

			}
		}
		else{
			System.out.println("There is no Sla to delete");
		}
	}


	public static void deleteAllMechanismsFromServiceManager(){
		Client client = Client.create();

		WebResource webResource = client
				.resource(BASE_PATH+SERVICE_MANAGER_PATH);

		ClientResponse response = webResource
				.get(ClientResponse.class);

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}

		String xml = response.getEntity(String.class);

		System.out.println("Output from Server .... \n");
		System.out.println(xml);

		CollectionSchemaMetrics collection = new Gson().fromJson(xml, CollectionSchemaMetrics.class);
		if(collection!=null){
			for(int i=0;i<collection.getItem().size();i++){
				WebResource webResource2 = client
						.resource(collection.getItem().get(i));

				ClientResponse response2 = webResource2.delete(ClientResponse.class);

				if (response2.getStatus() != 200) {
					throw new RuntimeException("Failed : HTTP error code : "
							+ response2.getStatus());
				}
				System.out.println("Deleted mechanism: "+collection.getItem().get(i));
			}
		}



	}


	public static void deleteAllSupplyChains(){
		Client client = Client.create();

		WebResource webResource = client
				.resource(BASE_PATH+SUPPLY_CHAIN_PATH);

		ClientResponse response = webResource.accept("application/json")
				.get(ClientResponse.class);

		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}

		String xml = response.getEntity(String.class);

		SupplyChainCollection supplyCOllection = new Gson().fromJson(xml, SupplyChainCollection.class);
		System.out.println("supplyCOllection: "+supplyCOllection);

		for(int i=0;i<supplyCOllection.getItem_list().length;i++){
			if(supplyCOllection.getItem_list()[i].getId().equalsIgnoreCase("bd36fcc6-54be-4104-9458-72a39f90ebb2")){
				System.out.println("the SC with id "+supplyCOllection.getItem_list()[i].getId()+" has not to be deleted");
			}
			else{
				Client client2 = Client.create();

				WebResource webResource2 = client2.resource(supplyCOllection.getItem_list()[i].getItem());

				ClientResponse response2 = webResource2.delete(ClientResponse.class);
				System.out.println("response2: "+response2);

				// the server replies 204 (No Content) if the SC is correctly deleted 
				if (response2.getStatus() != 204) {
					System.out.println("error: "+response2.getStatus());
					throw new RuntimeException("Failed : HTTP error code : "
							+ response2.getStatus());
				}
			}


		}
	}


	public static void deleteAllPlanActivites(){
		Client client = Client.create();

		System.out.println("deleting plan activities");
		WebResource webResource = client
				.resource(BASE_PATH+PLAN_ACTIVITIES_PATH);

		ClientResponse response = webResource.accept("application/json")
				.get(ClientResponse.class);

		if (response.getStatus() != 200) {
			System.out.println("failed to cancel plan activities");
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}

		String xml = response.getEntity(String.class);

		PlanActivityCollection planActivityCllection = new Gson().fromJson(xml, PlanActivityCollection.class);

		if(planActivityCllection.getItem_list()!=null && planActivityCllection.getItem_list().length>0){
			for(int i=0;i<planActivityCllection.getItem_list().length;i++){

				if(planActivityCllection.getItem_list()[i].getId().equalsIgnoreCase("3cedef57-731e-4d6c-9427-25ce0ba28675")){
					System.out.println("the PA with id "+planActivityCllection.getItem_list()[i].getId()+" has not to be deleted.");
				}
				else{
					System.out.println("deleting PA with id "+planActivityCllection.getItem_list()[i].getId());
					Client client2 = Client.create();

					WebResource webResource2 = client2.resource(planActivityCllection.getItem_list()[i].getItem());

					ClientResponse response2 = webResource2.delete(ClientResponse.class);

					// the server replies 204 (No Content) if the SC is correctly deleted 
					if (response2.getStatus() != 204) {
						throw new RuntimeException("Failed : HTTP error code : "
								+ response2.getStatus());
					}
				}


			}
		}
		else{
			System.out.println("no plan activity to delete");
		}
	}


	private static Collection buildCollectionFromXml(String xml) {
		Collection collection = null;
		try {
			JAXBContext jc = JAXBContext.newInstance(Collection.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			collection = (Collection) unmarshaller
					.unmarshal(new StringReader(xml));
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return collection;
	}


	private static eu.specsproject.slo.Collection buildCollectionSlosFromXml(String xml) {
		eu.specsproject.slo.Collection collection = null;
		try {
			JAXBContext jc = JAXBContext.newInstance(eu.specsproject.slo.Collection.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			collection = (eu.specsproject.slo.Collection) unmarshaller
					.unmarshal(new StringReader(xml));
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return collection;
	}


	private static AgreementOffer buildSla(String xml) {
		AgreementOffer offer = null;
		try {
			JAXBContext jc = JAXBContext.newInstance(AgreementOffer.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			offer = (AgreementOffer) unmarshaller
					.unmarshal(new StringReader(xml));
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return offer;
	}

	private static StringWriter getStringFromAgreementOffer(AgreementOffer value){
		try {
			Result returnValue = null;
			JAXBContext jaxbContext = JAXBContext.newInstance(AgreementOffer.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			StringWriter writer = new StringWriter();
			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			jaxbMarshaller.marshal(value, writer);
			//			jaxbMarshaller.marshal(value, System.out);
			return writer;

		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}
	}

}
